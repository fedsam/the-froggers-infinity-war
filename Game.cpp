﻿#include "Game.h"
#include "FireBall.h"

Game::Game(QWidget *parent) : QGraphicsView(parent)
{

    // Window config
    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setFixedSize(WINDOW_WIDTH, WINDOW_HEIGHT);
    setSceneRect(QRectF(0,0, WINDOW_WIDTH, WINDOW_HEIGHT));
    setInteractive(false);



}
void Game::start(){
    // Create  & set the scene to the view

    scene_ = new Scene(this);

    setScene(scene_);

    // left right movement timer
    move_timer_.setTimerType(Qt::PreciseTimer);
    move_timer_.setInterval(25);
    connect(&move_timer_, SIGNAL(timeout()), this, SLOT(move()));

    health_timer.setTimerType(Qt::PreciseTimer);
    health_timer.setInterval(5);
    connect(&health_timer,SIGNAL(timeout()),this,SLOT(isGameOver()));
    health_timer.start();

    // 60 FPS  gravity timer
    gravity_timer_.setTimerType(Qt::PreciseTimer);
    gravity_timer_.setInterval(20);
    connect(&gravity_timer_, SIGNAL(timeout()), this, SLOT(gravity()));
    gravity_timer_.start();

    // Create jump animation
    jumpAnimation_ = new QPropertyAnimation(this);
    jumpAnimation_->setTargetObject(this);
    jumpAnimation_->setPropertyName("jumpCoeff");
    jumpAnimation_->setStartValue(0);
    jumpAnimation_->setKeyValueAt(0.5, 1);
    jumpAnimation_->setEndValue(0);
    jumpAnimation_->setDuration(800);
    jumpAnimation_->setEasingCurve(QEasingCurve::OutInQuad);
    connect(this, SIGNAL(jumpCoeffChanged(qreal)), this, SLOT(jump()));
    this->setEnabled(true);
}
Game::~Game(){}

void Game::keyPressEvent(QKeyEvent *event)
{
    if (event->isAutoRepeat()){
           return;}

    switch (event->key()) {
    case Qt::Key_Left:

        scene_->getPlayer()->setState(-1);
        scene_->getPlayer()->setIs_Left(true);
        if (scene_->getPlayer()->getState() == 0){
            move_timer_.stop();
        }
        else if (!move_timer_.isActive()){
            move_timer_.start();

        }

        break;
    case Qt::Key_Right:
        scene_->getPlayer()->setState(1);
        scene_->getPlayer()->setIs_Left(false);
        if (scene_->getPlayer()->getState() == 0){
            move_timer_.stop();
        }
        else if (!move_timer_.isActive()){
            move_timer_.start();

        }

        break;
    case Qt::Key_Space:

        // If the player already is jumping
        if(gravity_timer_.isActive()) return;
        if (jumpAnimation_->state() == QAbstractAnimation::Stopped){
            jumpAnimation_->start();}

        break;
    case Qt::Key_V:

        scene_->addItem(scene_->getPlayer()->throwFireball());
        break;

    }
}


void Game::keyReleaseEvent(QKeyEvent *event)
{
    if (event->isAutoRepeat())
           return;

    switch (event->key()) {
    case Qt::Key_Left:
        scene_->getPlayer()->setState(1);
        scene_->getPlayer()->setIs_Left(true);
        if (scene_->getPlayer()->getState() == 0){
            move_timer_.stop();
        }
        else if (!move_timer_.isActive()){
            move_timer_.start();

        }

        break;
    case Qt::Key_Right:
        scene_->getPlayer()->setState(-1);
        scene_->getPlayer()->setIs_Left(false);
        if (scene_->getPlayer()->getState() == 0){
            move_timer_.stop();
        }
        else if (!move_timer_.isActive()){
            move_timer_.start();

        }

        break;
    case Qt::Key_Space:
        scene_->getPlayer()->setJumpingState(false);

        break;

    }
}

void Game::handleEvents(){}

void Game::gameOver()
{
    displayGameOver();
}

void Game::displayGameOver()
{

    for (size_t i = 0, n = scene_->items().size(); i < n; i++){
           scene_->items()[i]->setEnabled(false);
    }
    scene_->items().clear();
    scene_->blockSignals(true);
    scene_->clear();
    scene_->blockSignals(false);



   // this->setEnabled(false);

    //this->jumpAnimation_->stop();

   drawPanel(0,0,1024,768,Qt::black,0.65);

        // draw panel
   drawPanel(250,184,400,250,Qt::lightGray,0.40);


    this->setEnabled(false);

    this->gravity_timer_.stop();
    this->move_timer_.stop();
    disconnect(&move_timer_, SIGNAL(timeout()), 0, 0);
    disconnect(&gravity_timer_, SIGNAL(timeout()), 0, 0);




    //start();

    this->setEnabled(true);
    Button* playAgain = new Button(QString("Play again"));
    playAgain->setPos(410,300);
    scene_->addItem(playAgain);
    connect(playAgain,SIGNAL(clicked()),this,SLOT(restartGame()));



}
void Game::restartGame(){
  qDebug() << "Start" << endl;
  start();
}
void Game::gravity()
{
    // The players falls
    scene_->getPlayer()->setPos(scene_->getPlayer()->x(),scene_->getPlayer()->y() + 20);

    // If the player is touching something while falling
    if(handleCollisionWhileFalling()){
        qDebug() << " gravity : la gravité s'arrête car on touche une plateforme en tombant " << endl;
        gravity_timer_.stop();

    }
    // when the player is arriving beneath the ground
    else if(scene_->getPlayer()->y() + scene_->getPlayer()->boundingRect().height() >= SCENE_GROUND_HEIGHT){
        qDebug() << " gravity : la gravité s'arrête car on touche le sol " << endl;
         scene_->getPlayer()->setPos(scene_->getPlayer()->x(), SCENE_GROUND_HEIGHT - scene_->getPlayer()->boundingRect().height());
         gravity_timer_.stop();
         scene_->getPlayer()->setTouchedPlatform(0);
    }

}
void Game::isGameOver(){
    if(scene_->getHealth()->getHealthNb() < 0) {
        //qDebug() << "GAME OVER" << endl;
        gameOver();
    }
}
void Game::jump()
{
    //checkIfTheFloorIsLava();
   if(jumpAnimation_->state() == QAbstractAnimation::Stopped){
       qDebug() << "WOW ENFIN UTILE DANS JUMP"<< endl;
       return;}

   QGraphicsItem* item = scene_->collidingPlatforms();
   if(item){
//       if(scene_->getPlayer()->isTouchingFeet(item)){
//           qreal y = (GROUND_HEIGHT  -  scene_->getPlayer()->y()  -  scene_->getPlayer()->boundingRect().height()) - jumpAnimation_->currentValue().toReal() * JUMP_HEIGHT;
//           scene_->getPlayer()->setPos(scene_->getPlayer()->x(), y);
//           return;
//       }
       // if the head is touching the platform
       if(scene_->getPlayer()->isTouchingHead(item)){
           qDebug() << " la tête touche, l'animation stoppée" << endl;
           jumpAnimation_->stop();

           // if still touching the platform the player goes over the platform
//            if(scene_->getPlayer()->getTouchedPlatform()){
//                 qDebug() << "projeté au-dessus de la plateforme"<< endl;
//                 scene_->getPlayer()->setPos(scene_->getPlayer()->x(), scene_->getPlayer()->getTouchedPlatform()->y() - scene_->getPlayer()->boundingRect().height());
//                 return;
//             }
           // if not the player goes back to the ground
           if(!scene_->getPlayer()->isTouchingPlatform(item)){
               qDebug() << "projeté en-dessous de la plateforme au sol"<< endl;
               scene_->getPlayer()->setPos(scene_->getPlayer()->x(), GROUND_HEIGHT - scene_->getPlayer()->boundingRect().height());
               return;
           }
       }
       // if not we handle the collision while falling
       else{
           if(handleCollisionWhileFalling()){
               return;
           }
       }
   }

   if(gravity_timer_.isActive()){
       return;
   }

   //On saute pour atterir sur une plateforme
//     QGraphicsItem* item = scene_->collidingPlatforms();
//     if(item){
//         if(! scene_->getPlayer()->getTouchedPlatform()){
//            handleCollisionWhileFalling();
//         }
//     }
   //int prout = scene_->getPlayer()->getTouchedPlatform()->boundingRect().height();

   qreal y = (GROUND_HEIGHT  - scene_->getPlayer()->boundingRect().height()) - jumpAnimation_->currentValue().toReal() * JUMP_HEIGHT;
   scene_->getPlayer()->setPos(scene_->getPlayer()->x(), y);


}

void Game::move()
{
        qDebug() << scene_->getHealth()->getHealthNb() << endl;


        checkIfTheFloorIsLava();

        // If falling then no moving
        if(scene_->getPlayer()->getIs_falling()){
            qDebug() << "move : NON ! si en train de tomber on bouge pas"<< endl;
            return;
        }
        //If advance and it's empty
//        if(!(scene_->getPlayer()->getTouchedPlatform() && scene_->getPlayer()->isTouchingPlatform(scene_->getPlayer()->getTouchedPlatform()))&& jumpAnimation_->state() == QAbstractAnimation::Stopped){
//            if(scene_->getPlayer()->getTouchedPlatform()){
//                qDebug() << "move : TOMBER DANS LE VIDE"<< endl;
//                scene_->getPlayer()->setIs_falling(true);
//                gravity_timer_.start();
//            }
//        }
        if(scene_->getPlayer()->getTouchedPlatform() == 0  && scene_->getPlayer()->y() < GROUND_HEIGHT - scene_->getPlayer()->boundingRect().height() && jumpAnimation_->state() == QAbstractAnimation::Stopped){
            qDebug() << "move : t'avance dans le vide coco "<< endl;
            gravity_timer_.start();
        }

        if (scene_->handleCollisionWithRectItems()) return;

        // Left
        if (scene_->getPlayer()->getState() < 0)
        {

            if (scene_->getPlayer()->pos().x() - 32 > 0) scene_->getPlayer()->moveBy(- 5, 0);
        }
        // Right
        else if (scene_->getPlayer()->getState() > 0)
        {

            if (scene_->getPlayer()->pos().x() + 32 < SCENE_WIDTH) scene_->getPlayer()->moveBy(5, 0);
        }

        scene_->getPlayer()->nextFrame();
        moveView();


}

void Game::moveView(){
    if(scene_->getPlayer()->x() > SCENE_WIDTH - WINDOW_WIDTH/2 - 42 || scene_->getPlayer()->x() <  WINDOW_WIDTH/2 - 42  )
        return;
    scene_->getHealth()->setPos(scene_->getPlayer()->x() - WINDOW_WIDTH/2 + 42, 0);
    setSceneRect((scene_->getPlayer()->x() - WINDOW_WIDTH/2 + 42),0, WINDOW_WIDTH, WINDOW_HEIGHT);


}


bool Game::handleCollisionWhileFalling()
{

    QGraphicsItem* platform =  scene_->collidingPlatforms();
    // Si on touche une plateforme
    if(platform) {
        if(scene_->getPlayer()->isTouchingFeet(platform) && scene_->getPlayer()->isTouchingPlatform(platform) ){
             qDebug() << "handleCollisionWhileFalling : CA TOUCHE LES PIEDS WALLAH" << endl;
             scene_->getPlayer()->setTouchedPlatform(platform);
             jumpAnimation_->stop();
             return true;
         }

    }
    // Sinon rien
    else {
         scene_->getPlayer()->setTouchedPlatform(0);

         qDebug() << "handleCollisionWhileFalling : on handle et y'a rien niquel" << endl;
    }
    return false;
}




qreal Game::jumpCoeff() const
{
    return jumpCoeff_;
}

void Game::setJumpCoeff(const qreal &jumpCoeff)
{
    if (jumpCoeff_ == jumpCoeff)
        return;

    jumpCoeff_ = jumpCoeff;
    emit jumpCoeffChanged(jumpCoeff_);
}
void Game::checkIfTheFloorIsLava(){
    QList<QGraphicsItem*> items =  scene_->getPlayer()->collidingItems();
    for (int i = 0; i< items.size(); i++){
        if ( typeid (*items[i]) == typeid (Ground))
            scene_->getHealth()->setHealthNb(scene_->getHealth()->getHealthNb() - 1);

    }
}
void Game::drawPanel(int x, int y, int width, int height, QColor color, double opacity){
    // draws a panel at the specified location with the specified properties
    QGraphicsRectItem* panel = new QGraphicsRectItem(x,y,width,height);
    QBrush brush;
    brush.setStyle(Qt::SolidPattern);
    brush.setColor(color);
    panel->setBrush(brush);
    panel->setOpacity(opacity);
    scene_->addItem(panel);
}
/*
void Game::render()
{
}

void Game::clear()
{
}
*/
