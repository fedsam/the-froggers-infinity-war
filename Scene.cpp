#include "Scene.h"



Scene::Scene(QObject *parent)
    :QGraphicsScene(parent)
{
    //clear();
    sprite_image_ = new QPixmap(":/sprites/tileset.png");

    // Set the scene
    setSceneRect(0, 0, SCENE_WIDTH, SCENE_HEIGHT);


    // Create & add the player
    player_ = new Player();
    player_->setPos(800 / 2 - 42, SCENE_HEIGHT / 2);
    addItem(player_);

    // Create & add Health
    health_ = new Health();
    addItem(health_);
    ground = new Ground();
    ground->setPos(200, SCENE_GROUND_HEIGHT - 66);
    addItem(ground);


    //Create & add 4  rect platforms
    rectplatforms_.push_back(new RectPlatform(100, SCENE_GROUND_HEIGHT - 70, 120, 30));
    rectplatforms_.push_back(new RectPlatform(600, SCENE_GROUND_HEIGHT - 60, 100, 50));
    rectplatforms_.push_back(new RectPlatform(811, SCENE_GROUND_HEIGHT - 80, 100, 70));
    rectplatforms_.push_back(new RectPlatform(1000, SCENE_GROUND_HEIGHT - 40, 100, 20));

    addItem(rectplatforms_[0]);
    addItem(rectplatforms_[1]);
    addItem(rectplatforms_[2]);
    addItem(rectplatforms_[3]);
}


QGraphicsItem *Scene::collidingPlatforms()
{

    QList<QGraphicsItem*> items =  player_->collidingItems();
    for (int i = 0; i< items.size(); i++){
        if ( typeid (*items[i]) == typeid (RectPlatform)){
            //qDebug() << " hey y'a un rectPlatform de touché " << endl;
            return items[i];
        }
    }


    return 0;
}




bool Scene::handleCollisionWithRectItems()
{

    QGraphicsItem * item = collidingPlatforms();

    if(item){

        player_->setTouchedPlatform(item);
        //to the right
        if (!player_->getIs_Left()){



            if(player_->x() >= item->boundingRect().toRect().x()){
                return false;}

            if (player_->isTouchingPlatform(item) && player_->isTouchingFeet(item) ){

                return false;
            }



        }
        //to the left
        else if (player_->getIs_Left() ){




            if(player_->x() <= item->boundingRect().toRect().x()){
                return false;
            }
            if (player_->isTouchingPlatform(item) && player_->isTouchingFeet(item) ){

                return false;
            }


        }
        return true;

    }
    player_->setTouchedPlatform(0);
    return false;
}


void Scene::drawBackground(QPainter *painter, const QRectF &rect)
{
    Q_UNUSED(rect);

    // Add sky
    painter->fillRect(
                QRectF(0, 0, width(), height()),
                QImage(":/sprites/sky.png"));

    // Add clouds
    painter->drawImage(
                QRectF(0, height() * 0.4, width(), height() / 2),
                QImage(":/sprites/clouds.png"));

    // Add sea
    painter->drawImage(
                QRectF(0, height() * 0.7, width(), height()),
                QImage(":/sprites/sea.png"));

    // Add ground
    painter->fillRect(
                QRectF(0, height() * 0.9, width() , height() * 0.1),
                sprite_image_->copy(QRect(193, 183, 46, 40)));
}






